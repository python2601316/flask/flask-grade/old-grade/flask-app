import sqlite3


class SQLiteDBStorage:
    def __init__(self, path):
        self.__db_path = path
        self.create_table()

    def write_to(self):
        self._db_conn.commit()

    def load_from(self):
        return self._make_select_all_query()

    def get_row_by_id(self, id):
        return self._make_select_row_by_id_query(id)

    def import_data(self, data):
        self._make_insert_many_rows_query(data)

    def add_row(self, model):
        self._make_insert_row_query(model)

    def update_row(self, data):
        self._make_update_row_query(data)

    def delete_row(self, id):
        self._make_delete_row_by_id_query(id)

    def clear_table(self):
        self._make_delete_all_query()

    def _make_insert_row_query(self, data):
        try:
            self._open_connection()
            self._db_cursor.execute("insert into publications values(NULL, ?, ?, ?, ?, ?, ?, ?)", data)
        except sqlite3.DatabaseError as err:
            print("Error: ", err)
        else:
            self.write_to()
            self._close_connection()

    def _make_insert_many_rows_query(self, data):
        print(data)
        try:
            self._open_connection()
            self._db_cursor.executemany("insert into publications values (?, ?, ?, ?, ?, ?, ?, ?)", data)
        except sqlite3.DatabaseError as err:
            print("Error: ", err)
        else:
            self.write_to()
            self._close_connection()

    def _make_update_row_query(self, data):
        try:
            self._open_connection()
            self._db_cursor.execute(
                "update publications set type=?, title=?, text=?, published_at=?, source=?, author=?, out_date=? where id=?",
                data
            )
        except sqlite3.DatabaseError as err:
            print("Error: ", err)
        else:
            self.write_to()
            self._close_connection()

    def _make_select_all_query(self):
        try:
            self._open_connection()
            self._db_cursor.execute("select * from publications")
            result = self._db_cursor.fetchall()
            self._close_connection()
            return result
        except sqlite3.DatabaseError as err:
            print("Error: ", err)

    def _make_select_row_by_id_query(self, id):
        try:
            self._open_connection()
            self._db_cursor.execute("select * from publications where id=? LIMIT 1", (id,))
            result = self._db_cursor.fetchone()
            self._close_connection()
            return result
        except sqlite3.DatabaseError as err:
            print("Error: ", err)

    def _make_delete_row_by_id_query(self, id):
        self._open_connection()
        self._db_cursor.execute("delete from publications where id=?", (id,))
        self.write_to()
        self._close_connection()


    def _make_delete_all_query(self):
        try:
            self._open_connection()
            self._db_cursor.execute("delete from publications")
        except sqlite3.DatabaseError as err:
            print("Error: ", err)
        else:
            self.write_to()
            self._close_connection()

    def create_table(self):
        try:
            self._open_connection()
            self._db_cursor.execute("""
                create table if not exists publications(
                   id INTEGER PRIMARY KEY AUTOINCREMENT,
                   type TEXT NOT NULL,
                   title TEXT NOT NULL,
                   text TEXT NOT NULL,
                   published_at DATETIME,
                   source TEXT,
                   author TEXT,
                   out_date DATETIME
            )""")
        except sqlite3.DatabaseError as err:
            print("Error: ", err)
        else:
            self.write_to()
            self._close_connection()

    def _open_connection(self):
        self._db_conn = sqlite3.connect(self.__db_path, detect_types=sqlite3.PARSE_DECLTYPES)
        self._db_conn.row_factory = sqlite3.Row
        self._db_cursor = self._db_conn.cursor()

    def _close_connection(self):
        self._db_conn.close()
