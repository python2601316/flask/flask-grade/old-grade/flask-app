import pickle


class PickleStorage:
    def __init__(self, path):
        self.__path = path

    def load_from(self):
        with open(self.__path, 'rb') as file_storage:
            return pickle.load(file_storage)

    def write_to(self, entities):
        with open(self.__path, 'wb') as file_storage:
            pickle.dump(entities, file_storage)
