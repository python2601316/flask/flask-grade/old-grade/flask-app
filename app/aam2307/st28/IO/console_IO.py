class ConsoleIO:
    def load_from(self, prompt):
        return input(prompt)

    def write_to(self, entity):
        print(entity)
