from flask import request


class ApiIO:
    def load_from(self):
        return request.get_json()

    def write_to(self, text):
        return text
