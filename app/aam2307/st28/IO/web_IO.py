from flask import request


class WebIO:
    def load_from(self):
        return request.form.to_dict()

    def write_to(self, text):
        return text
