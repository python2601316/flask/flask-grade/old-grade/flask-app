from flask import Blueprint, render_template, url_for, redirect, jsonify, Flask, request, Response

if __name__ == "__main__":
    from IO.web_IO import WebIO
    from IO.pickle_storage_IO import PickleStorage
    from MediaResource.media_resource import MediaResource
    from IO.db_storage import SQLiteDBStorage
    from IO.api_IO import ApiIO
else:
    from .IO.web_IO import WebIO
    from .IO.pickle_storage_IO import PickleStorage
    from .MediaResource.media_resource import MediaResource
    from .IO.db_storage import SQLiteDBStorage
    from .IO.api_IO import ApiIO

app = Flask(__name__)

bp = Blueprint('st0728', __name__)

io = WebIO()
rest_io = ApiIO()
io_storage = PickleStorage('data/aam2307/st28/data.pickle')
db_storage = SQLiteDBStorage('data/aam2307/st28/db.sqlite')
media = MediaResource(io=io, io_storage=io_storage, db_storage=db_storage)
rest_media = MediaResource(io=rest_io, io_storage=io_storage, db_storage=db_storage)


@bp.route("/", methods=['GET'])
def feed():
    return render_template(
        'aam2307/st28/list.html',
        feed=media.list()
    )


@bp.route("/feed/<int:id>", methods=['GET'])
def editForm(id):
    return render_template(
        'aam2307/st28/edit_form.html',
        item=media.find_publication_by_id(id),
    )


@bp.route("/feed/<int:id>", methods=['POST'])
def updateItem(id):
    media.edit_publication(id, request.form.to_dict())
    return redirect(url_for('st0728.feed'))


@bp.route("/feed-delete/<int:id>", methods=['GET'])
def deleteItem(id):
    media.delete_publication(id)
    return redirect(url_for('st0728.feed'))


@bp.route("/feed-save", methods=['GET'])
def saveFeed():
    media.write_to()
    return redirect(url_for('st0728.feed'))


@bp.route("/feed-load", methods=['GET'])
def loadFeed():
    media.load_from()
    return redirect(url_for('st0728.feed'))


@bp.route("/feed-clear", methods=['GET'])
def clearFeed():
    media.clear()
    return redirect(url_for('st0728.feed'))


@bp.route("/publish/<string:type>", methods=['GET'])
def actionForm(type):
    return render_template(
        'aam2307/st28/action_form.html',
        type=type,
    )


@bp.route("/publish/", methods=['POST'])
def publish():
    media.add_publication(request.form.to_dict())
    return redirect(url_for('st0728.feed'))


@bp.route("/api/feed", methods=['GET'])
def apiGetFeed():
    result = list(map(make_public_format, media.list()))
    return jsonify({
        'list': result,
    })


@bp.route("/api/feed/<int:id>", methods=['GET'])
def apiGetPublicationById(id):
    publication = media.find_publication_by_id(id)
    formatted_pub = make_public_format(media.find_publication_by_id(id)) if publication else None
    return jsonify({
        'item': formatted_pub
    })


@bp.route("/api/feed/", methods=['POST'])
def apiMakePublish():
    result = rest_media.add_publication(request.get_json())
    return jsonify({
        'success': result
    })


@bp.route("/api/feed/<int:id>", methods=['POST'])
def apiUpdatePublicationById(id):
    result = rest_media.edit_publication(id, request.get_json())
    return jsonify({
        'success': result
    })


@bp.route("/api/feed/<int:id>", methods=['DELETE'])
def apiDeletePublicationById(id):
    return jsonify({
        'success': media.delete_publication(id)
    })


@bp.route("/api/clear-feed", methods=['DELETE'])
def apiClearFeed():
    result = media.clear()
    return jsonify({
        'success': result
    })


def make_public_format(model):
    return {
        'id': model.get_id(),
        'type': model.get_type(),
        'title': model.get_title(),
        'text': model.get_text(),
        'published_at': model.get_published_at() if hasattr(model, 'get_published_at') else None,
        'source': model.get_source() if hasattr(model, 'get_source') else None,
        'author': model.get_author() if hasattr(model, 'get_author') else None,
        'out_date': model.get_out_date() if hasattr(model, 'get_out_date') else None,
    }


app.register_blueprint(bp)
