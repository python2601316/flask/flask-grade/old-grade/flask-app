class Publication:
    TABLE_NAME = 'publications'

    def __init__(
        self,
        id,
        type,
        title,
        text
    ):
        self._id = id
        self._type = type
        self._title = title
        self._text = text

    def to_tuple(self):
        return (
            self.get_id(),
            self.get_type(),
            self.get_title(),
            self.get_text(),
        )

    def to_dict(self):
        return {
            'id': self.get_id(),
            'type': self.get_type(),
            'title': self.get_title(),
            'text': self.get_text(),
        }

    def get_title(self):
        return self._title

    def get_type(self):
        return self._type

    def get_text(self):
        return self._text

    def get_id(self):
        return self._id
