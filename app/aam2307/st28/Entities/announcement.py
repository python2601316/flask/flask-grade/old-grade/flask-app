from .publication import Publication


class Announcement(Publication):
    def __init__(self, id, type, title, text, out_date):
        super().__init__(id, type, title, text)
        self._out_date = out_date

    @staticmethod
    def make_from_dict(r):
        return Announcement(
            r['id'],
            r['type'],
            r['title'],
            r['text'],
            r['out_date'],
        )

    def to_tuple(self):
        base_tuple = super().to_tuple()
        return base_tuple.__add__((
            None,
            None,
            None,
            self.get_out_date(),
        ))

    def to_dict(self):
        return super().to_dict().update({
            'out_date': self.get_out_date(),
        })

    def edit(self):
        super().edit()
        self.set_out_date()

    def get_out_date(self):
        return self._out_date

    def set_out_date(self):
        self._out_date = self._io_type.load_from('out_date')
