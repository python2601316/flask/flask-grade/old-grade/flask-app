from .publication import Publication


class Article(Publication):
    def __init__(self, id, type, title, text, author):
        super().__init__(id, type, title, text)
        self._author = author

    @staticmethod
    def make_from_dict(r):
        return Article(
            r['id'],
            r['type'],
            r['title'],
            r['text'],
            r['author'],
        )

    def to_tuple(self):
        base_tuple = super().to_tuple()
        return base_tuple.__add__((
            None,
            None,
            self.get_author(),
            None,
        ))

    def to_dict(self):
        return super().to_dict().update({
            'author': self.get_author(),
        })

    def edit(self):
        super().edit()
        self.set_author()

    def set_author(self):
        self._author = self._io_type.load_from('author')

    def get_author(self):
        return self._author
