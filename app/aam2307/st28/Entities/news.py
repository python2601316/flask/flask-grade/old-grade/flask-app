from .publication import Publication


class News(Publication):
    def __init__(self, id, type, title, text, published_at, source):
        super().__init__(id, type, title, text)
        self._published_at = published_at
        self._source = source

    @staticmethod
    def make_from_dict(r):
        return News(
            r['id'],
            r['type'],
            r['title'],
            r['text'],
            r['published_at'],
            r['source'],
        )

    def to_tuple(self):
        return super().to_tuple().__add__((
            self.get_published_at(),
            self.get_source(),
            None,
            None,
        ))

    def to_dict(self):
        return super().to_dict().update({
            'published_at': self.get_published_at(),
            'source': self.get_source()
        })

    def edit(self):
        super().edit()
        self.set_source()
        self.set_published_at()

    def set_published_at(self):
        self._published_at = self._io_type.load_from('published_at')

    def get_published_at(self):
        return self._published_at

    def set_source(self):
        self._source = self._io_type.load_from('source')

    def get_source(self):
        return self._source
