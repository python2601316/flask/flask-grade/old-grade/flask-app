from ..Entities.announcement import Announcement
from ..Entities.article import Article
from ..Entities.news import News
from ..Entities.publication import Publication


class MediaResource:
    def __init__(self, io, io_storage, db_storage):
        self.__publications: list[any] = []
        self.__last_id = 0
        self._io = io
        self._io_storage = io_storage
        self._db_storage = db_storage

    def get_db_object(self):
        return self._db_storage

    def list(self):
        self._init_publications_list()
        return self.__publications

    def add_publication(self, values):
        try:
            self._db_storage.add_row(self.__collect_insert_values(values))
            self.__last_id += 1
            return True
        except:
            return False

    def find_publication_by_id(self, id):
        return self.__make_publication(self._db_storage.get_row_by_id(id))

    def edit_publication(self, id, values):
        try:
            self._db_storage.update_row(self.__collect_insert_values(values) + (id,))
            return True
        except:
            return False

    def delete_publication(self, id):
        self._init_publications_list()

        if self.__search__by_id(id):
            self._db_storage.delete_row(id)
            return True
        else:
            return False

    def load_from(self):
        import_data = []
        for item in self._io_storage.load_from():
            import_data.append(item.to_tuple())
        self._db_storage.import_data(import_data)

    def write_to(self):
        try:
            self._io_storage.write_to(self.__publications)
            return True
        except:
            return False

    def clear(self):
        try:
            self.__publications = []
            self._db_storage.clear_table()
            return True
        except:
            return False

    def _init_publications_list(self):
        self.__publications = []
        for row in self._db_storage.load_from():
            self.__add_publication(self.__make_publication(row))

    def __make_publication(self, row):
        if row != None:
            if row['type'] == 'news':
                publication = News.make_from_dict(row)
            elif row['type'] == 'article':
                publication = Article.make_from_dict(row)
            else:
                publication = Announcement.make_from_dict(row)
            return publication
        return row

    def __add_publication(self, entity):
        self.__publications.append(entity)

    def __search__by_id(self, id):
        for pub in self.__publications:
            if pub.get_id() == id:
                return True
            else:
                return False

    def __collect_insert_values(self, data):
        return (
            data['type'],
            data['title'],
            data['text'],
            data['published_at'] if 'published_at' in data else None,
            data['source'] if 'source' in data else None,
            data['author'] if 'author' in data else None,
            data['out_date'] if 'out_date' in data else None,
        )
