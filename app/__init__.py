from flask import Flask
from flask import jsonify
from flask import render_template

app = Flask(__name__)

app.secret_key = "0123456789"

# добавить импорт своего модуля по шаблону

from app.aam2307.st28 import bp as bp0728

# добавить пункт меню для вызова своего модуля по шаблону:
bps = [
    ["[2307-28] Скриплюк Виктор", bp0728],
]

for i, (title, bp) in enumerate(sorted(bps), start=1):
    app.register_blueprint(bp, url_prefix=f"/st{i}")


@app.route("/")
def index():
    return render_template("index.tpl", bps=sorted(bps))


@app.route("/api/", methods=['GET'])
def api():
    sts = []
    for i, (title, bp) in enumerate(sorted(bps), start=1):
        sts.append([i, title])
    return jsonify({'sts': sts})
