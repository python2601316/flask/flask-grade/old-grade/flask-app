# добавить импорт своего модуля по шаблону
# import clients.asm<код группы>.st<номер по журналу>.main

import clients.aam2307.st28.main

MENU = [
    ["[2307-28] Скриплюк Виктор", clients.aam2307.st28.main.main],
]


def menu():
    print("------------------------------")
    for i, item in enumerate(sorted(MENU)):
        print("{0:2}. {1}".format(i, item[0]))
    print("------------------------------")
    return int(input())

try:
    while True:
        sorted(MENU)[menu()][1]()
except Exception as ex:
    print(ex, "\nbye")
