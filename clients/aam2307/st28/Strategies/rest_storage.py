import json
import requests


class BaseApiClient:
    def __init__(self):
        self.__api_prefix = None
        self.__base_url = 'http://localhost:5000/'
        self.__st_n = self.__get_mine_prefix_number()
        self.__api_prefix = 'st' + str(self.__st_n) + '/api/'

    def send_request(self, method, endpoint="", data="", headers=None):
        if headers is None:
            headers = {}

        try:
            api_url = self.__base_url + self.__api_prefix if self.__api_prefix else self.__base_url

            if len(data):
                headers.update({"Content-type": 'application/json'})

            res = method(
                api_url + endpoint,
                headers=headers,
                data=json.dumps(data),
            )

            if res.status_code == 200:
                return json.loads(res.content)
        except Exception as e:
            pass

    def get_feed(self):
        return self.send_request(requests.get, 'feed')['list']

    def get_publication_by_id(self, id):
        return self.send_request(requests.get, f'feed/{id}')['item']

    def create_publication(self, publication, type):
        return self.send_request(requests.post, f'feed/{type}', publication)['success']

    def update_publication_by_id(self, values, id):
        return self.send_request(requests.post, f'feed/{id}', values)['success']

    def delete_publication_by_id(self, id):
        return self.send_request(requests.delete, f'feed/{id}')['success']

    def clear_feed(self):
        return self.send_request(requests.delete, 'clear-feed')['success']

    def __get_mine_prefix_number(self):
        stud_list = self.send_request(requests.get, 'api')
        for student in stud_list['sts']:
            if '[2307-28]' in student[1]:
                mine_num = student[0]
        return mine_num
