import pickle


class PickleStorage:
    def load_from(self):
        with open('data.pickle', 'rb') as file_storage:
            return pickle.load(file_storage)

    def write_to(self, entities):
        with open('data.pickle', 'wb') as file_storage:
            pickle.dump(entities, file_storage)
