class Menu:
    def __init__(self, media_resource, io_type):
        self._media_resource = media_resource
        self._io_type = io_type

        self.__actions = [
            ["Добавить", self._media_resource.add_entity],
            ["Редактировать", self._media_resource.edit_entity],
            ["Показать список", self._media_resource.list],
            ["Поиск по id", self._media_resource.show_element],
            ["Удалить элемент", self._media_resource.delete],
            ["Очистить", self._media_resource.clear],
            ["Загрузить", self._media_resource.load_from],
            ["Сохранить", self._media_resource.write_to],
        ]

    def show(self):
        while True:
            for i, menuItem in enumerate(self.__actions, 1):
                self._io_type.write_to(f"{i}. {menuItem[0]}")

            try:
                try:
                    m = int(self._io_type.load_from('Выберите действие: '))
                except:
                    raise ValueError('Некорректное действие')

                if m <= 0 or m > len(self.__actions):
                    raise ValueError('Некорректное действие')

                self.__actions[m - 1][1]()
            except Exception as e:
                self._io_type.write_to(e)

