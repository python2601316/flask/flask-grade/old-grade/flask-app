from ..Entities.announcement import Announcement
from ..Entities.article import Article
from ..Entities.news import News
from .base_media_resource import BaseMediaResource


class MediaResource(BaseMediaResource):
    def __init__(self, io_console, io_storage):
        self.__entities: list[any] = []
        self.__last_id = 0
        self._io_console = io_console
        self._io_storage = io_storage

    def list(self):
        if not self.__entities:
            raise Exception('Список пуст')
        else:
            for element in self.__entities:
                self._io_console.write_to(element)

    def add_entity(self):
        entity_type = int(self._io_console.load_from('Выберите тип публикации: 1 - Статья, 2 - Новость, 3 - Анонс: '))

        try:
            match entity_type:
                case 1:
                    self.__push_entity(Article(self._io_console, self.__last_id))
                case 2:
                    self.__push_entity(News(self._io_console, self.__last_id))
                case 3:
                    self.__push_entity(Announcement(self._io_console, self.__last_id))
                case _:
                    raise Exception()

            self.__last_id += 1
        except:
            raise Exception('Не удалось добавить объект')

    def show_element(self):
        self.__choose_object_id_for_action('show')

    def edit_entity(self):
        self.__choose_object_id_for_action('edit')

    def delete(self):
        self.__choose_object_id_for_action('delete')

    def __remove_entity_by_index(self, index):
        self.__entities.pop(index)
        self._io_console.write_to('Объект успешно удален')

    def load_from(self):
        self.__entities = self._io_storage.load_from()
        self.__last_id = self.__entities[len(self.__entities) - 1]._obj_id + 1
        self._io_console.write_to('Список успешно загружен')

    def write_to(self):
        if not self.__entities:
            raise Exception('Нечего сохранять')
        else:
            self._io_storage.write_to(self.__entities)
            self._io_console.write_to('Список успешно сохранен в файл')

    def clear(self):
        if not self.__entities:
            raise Exception('Список и так пуст')
        else:
            self.__entities = []
            self._io_console.write_to('Список успешно очищен')

    def __push_entity(self, entity):
        self.__entities.append(entity)

    def __choose_object_id_for_action(self, action):
        try:
            entity_id = int(self._io_console.load_from('Введите id элемента: '))
        except:
            raise ValueError('Id должно быть числом')

        found = False

        for index, entity in enumerate(self.__entities):
            if entity._obj_id == entity_id:
                found = True
                match action:
                    case 'edit':
                        entity.edit()
                    case 'delete':
                        self.__remove_entity_by_index(index)
                    case 'show':
                        self._io_console.write_to(entity)
                break
        if not found:
            raise Exception('Элемент с таким id не найден')
