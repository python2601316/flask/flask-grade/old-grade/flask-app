from abc import ABC, abstractmethod


class BaseMediaResource(ABC):
    @abstractmethod
    def list(self):
        pass

    @abstractmethod
    def add_entity(self):
        pass

    @abstractmethod
    def show_element(self):
        pass

    @abstractmethod
    def edit_entity(self):
        pass

    @abstractmethod
    def delete(self):
        pass

    @abstractmethod
    def load_from(self):
        pass

    @abstractmethod
    def write_to(self):
        pass

    @abstractmethod
    def clear(self):
        pass
