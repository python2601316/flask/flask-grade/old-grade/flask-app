from .base_media_resource import BaseMediaResource


class MediaResourceRestApiClient(BaseMediaResource):
    def __init__(self, io_console, base_api_client):
        self._io_console = io_console
        self._base_api_client = base_api_client

    def list(self):
        list = self._base_api_client.get_feed()
        if not list:
            self._io_console.write_to('Список пуст')
        for item in self._base_api_client.get_feed():
            self._io_console.write_to('____________')
            for field in item:
                self._io_console.write_to(field + ': ' + str(item[field]))
        self._io_console.write_to('____________')

    def show_element(self):
        id = int(self._io_console.load_from('Введите id элемента: '))
        item = self._base_api_client.get_publication_by_id(id)

        if item == 'Not Found':
            self._io_console.write_to('Не удалось найти публикацию')
        else:
            self._io_console.write_to('____________')
            for field in item:
                self._io_console.write_to(field + ': ' + str(item[field]))
            self._io_console.write_to('____________')

    def add_entity(self):
        publication = self.__add_or_update_console_dialog()

        if publication.get('source') or publication.get('published_at'):
            type = 'news'
        elif publication.get('out_date'):
            type = 'announcement'
        else:
            type = 'article'

        if self._base_api_client.create_publication(publication, type):
            self._io_console.write_to('Успешно опубликована')
        else:
            self._io_console.write_to('Не удалось опубликовать')

    def edit_entity(self):
        id = int(self._io_console.load_from('Введите id элемента: '))
        item = self._base_api_client.get_publication_by_id(id)

        if item == 'Not Found':
            self._io_console.write_to('Не удалось найти публикацию')
        else:
            self._io_console.write_to('____________')
            for field in item:
                self._io_console.write_to(field + ': ' + str(item[field]))
            self._io_console.write_to('____________')

            publication = self.__add_or_update_console_dialog()

            if self._base_api_client.update_publication_by_id(values=publication, id=item['id']):
                self._io_console.write_to('Публикация успешно обновлена')
            else:
                self._io_console.write_to('Не удалось обновить публикацию')

    def delete(self):
        id = self.__choose_object_id_for_action()
        self._base_api_client.delete_publication_by_id(id)

    def clear(self):
        if self._base_api_client.clear_feed():
            self._io_console.write_to('Лента успешно очищена')
        else:
            self._io_console.write_to('Не удалось очистить ленту')

    def write_to(self):
        self._io_console.write_to('Сервис пока не поддерживает этот функционал')

    def load_from(self):
        self.list()

    def __choose_object_id_for_action(self):
        try:
            return int(self._io_console.load_from('Введите id элемента: '))
        except:
            raise ValueError('Id должно быть числом')

    def __add_or_update_console_dialog(self):
        publication = {
            'title': None,
            'text': None,
            'author': None,
            'source': None,
            'published_at': None,
            'out_date': None,
        }

        publication['title'] = self._io_console.load_from('Заголовок: ')
        publication['text'] = self._io_console.load_from('Содержание: ')

        entity_type = int(self._io_console.load_from('Тип публикации: 1 - Статья, 2 - Новость, 3 - Анонс: '))

        try:
            match entity_type:
                case 1:
                    publication['author'] = self._io_console.load_from('Автор: ')
                case 2:
                    publication['source'] = self._io_console.load_from('Источник: ')
                    publication['published_at'] = self._io_console.load_from('Дата публикации: ')
                case 3:
                    publication['out_date'] = self._io_console.load_from('Дата анонса: ')
                case _:
                    raise Exception()
            return publication
        except:
            raise Exception('Некорректные данные')
