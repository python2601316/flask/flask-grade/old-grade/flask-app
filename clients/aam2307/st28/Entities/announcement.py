from .publication import Publication


class Announcement(Publication):
    def __init__(self, io_type, obj_id):
        super().__init__(io_type, obj_id)
        self._out_date = None
        self.set_out_date()

    def edit(self):
        super().edit()
        self.set_out_date()

    def get_out_date(self):
        return self._out_date

    def set_out_date(self):
        self._out_date = self._io_type.load_from('Дата выхода: ')

    def get_str(self):
        return " ".join([
            "id: ", str(self._obj_id), "\n",
            "Заголовок анонса: ", self.get_title(), "\n",
            "Содержание: ", self.get_text(), "\n",
            "Дата выхода: ", self.get_out_date(), "\n",
            "------------------------------------------------\n"
        ])
