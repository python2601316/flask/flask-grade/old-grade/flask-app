from .publication import Publication


class News(Publication):
    def __init__(self, io_type, obj_id):
        super().__init__(io_type, obj_id)
        self._published_at = None
        self._source = None
        self.set_published_at()
        self.set_source()

    def edit(self):
        super().edit()
        self.set_source()
        self.set_published_at()

    def set_published_at(self):
        self._published_at = self._io_type.load_from('Дата публикации: ')

    def get_published_at(self):
        return self._published_at

    def set_source(self):
        self._source = self._io_type.load_from('Первоисточник: ')

    def get_source(self):
        return self._source

    def get_str(self):
        return " ".join([
            "id: ", str(self._obj_id), "\n",
            "Заголовок новости: ", self.get_title(), "\n",
            "Содержание: ", self.get_text(), "\n",
            "Дата публикации: ", self.get_published_at(), "\n",
            "Первоисточник: ", self.get_source(), "\n",
            "------------------------------------------------\n"
        ])
