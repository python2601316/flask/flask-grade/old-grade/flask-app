from .publication import Publication


class Article(Publication):
    def __init__(self, io_type, obj_id):
        super().__init__(io_type, obj_id)
        self._author = None
        self.set_author()

    def edit(self):
        super().edit()
        self.set_author()

    def set_author(self):
        self._author = self._io_type.load_from('Автор: ')

    def get_author(self):
        return self._author

    def get_str(self):
        return " ".join([
            "id: ", str(self._obj_id), "\n",
            "Заголовок статьи: ", self.get_title(), "\n",
            "Содержание: ", self.get_text(), "\n",
            "Автор: ", self.get_author(), "\n",
            "------------------------------------------------\n"
        ])
