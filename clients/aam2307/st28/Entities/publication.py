class Publication:
    def __init__(self, io_type, obj_id):
        self._title = None
        self._text = None
        self._obj_id = obj_id
        self._io_type = io_type
        self.set_title()
        self.set_text()

    def edit(self):
        self.set_title()
        self.set_text()

    def get_title(self):
        return self._title

    def set_title(self):
        self._title = self._io_type.load_from('Заголовок: ')

    def get_text(self):
        return self._text

    def set_text(self):
        self._text = self._io_type.load_from('Содержание: ')

    def __str__(self):
        return self.get_str()
