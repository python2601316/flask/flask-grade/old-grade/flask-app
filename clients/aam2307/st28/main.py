if __name__ == '__main__':
    from .Menu.menu import Menu
    from .Strategies.console_IO import ConsoleIO
    from .Strategies.pickle_storage import PickleStorage
    from .MediaResource.media_resource_rest import MediaResourceRestApiClient
    from .MediaResource.media_resource import MediaResource
    from .Strategies.rest_storage import BaseApiClient
else:
    from .Menu.menu import Menu
    from .Strategies.console_IO import ConsoleIO
    from .Strategies.pickle_storage import PickleStorage
    from .MediaResource.media_resource_rest import MediaResourceRestApiClient
    from .MediaResource.media_resource import MediaResource
    from .Strategies.rest_storage import BaseApiClient


def main():
    io_console = ConsoleIO()
    # pickle_storage = PickleStorage()
    base_client = BaseApiClient()
    media = MediaResourceRestApiClient(io_console=io_console, base_api_client=base_client)
    # media = MediaResource(io_console=io_console, io_storage=pickle_storage)

    menu = Menu(media_resource=media, io_type=io_console)
    menu.show()


if __name__ == '__main__':
    main()
